#!/usr/bin/env ksh
# bash doesn't properly handle the exit in the while; use ksh

#	Mnemonic:	start_container.sh
#	Abstract:	This script sets up and starts a docker image that
#				is assumed to have a browser installed. By default
#				the directory /home/<USER>/gaolfs is assumed to be
#				the directory structure that should be mounted as
#				/home/foo in the container. This allows the browser
#				persistant files to span container invocations.
#
#				Environment variables that affect setup and start:
#					BRC_IMAGE the image name to use (vivaldi:u1804)
#					BRC_CMD the command to start the browser (vivaldi)
#					BRC_HOME the directory as the home volume given
#							to the container ($HOME/gaolfs/goober)
#
#				You must run `xhost +` to oepn the server up.
#
#	Date:		1 June 2020
#	Author:		E. Scott Daniels
# -------------------------------------------------------------------

image_name=${BRC_IMAGE:-vivaldi:u1804}
bcmd=${BRC_CMD}
homefs=${BRC_HOME:-$HOME/gaolfs/home/goober}
detached="--rm -d"
clean_only=0
verbose=0
cname="browser"

while [[ $1 == -* ]]
do
	case $1 in 
		-C)	clean_only=1;;
		-c)	bcmd=$2; shift;;
		-h)	homefs=$2; shift;;
		-i)	detached="--rm -it";;
		-I) image_name=$2; shift;;
		-N)	homefs="";;
		-n)	cname=$2; shift;;
		-v)	verbose=1;;

		*)	echo "unrecognised option: $1"
			echo "usage: $0 [-C] [-c browser-command] [-h home-fs] [-I image-name] [-i] [-N]"
			echo "	-C is clean only mode, no container is started"
			echo "	-N prevents a local filesystem from being mouted into /home"
			echo "	-i starts in interactive mode"
			exit 1
			;;
	esac

	shift
done

if [[ -n $1 ]]
then
	bcmd=$1
fi

# clean up exited images if they exist
docker ps -a| grep $cname | awk '
	/Exited/  { print $1; next }
	{ print "running" }
' |  while read id
do
	if [[ $id == "running" ]]
	then
		if (( ! clean_only ))
		then
			echo "looks like $cname container is already running:"
			docker ps -a|grep $cname
			exit 1			# not properly handled by some (all?) versions of bash; use ksh
		else
			echo "clean only mode: leaving running container alone:"
			docker ps -a|grep $cname
		fi
	else
		if (( clean_only ))
		then
			echo "removing exited container: $id"
		fi
		docker rm $id		# remove exited container
	fi
done

if (( clean_only ))
then
	exit 0
fi

# will cause the setup script to create the user foo with the matching uid value
uid="-e FOO_ID=$( id -u )"

if [[ $homefs != "" ]]
then
	homefs="-v $homefs:/home/foo"
fi

if (( verbose ))
then
	set -x
fi
docker run  $detached \
	--cap-add SYS_ADMIN \
	--ipc=host \
    --net host \
    --cpuset-cpus 0 \
    --memory 1024mb \
    -v /tmp/.X11-unix:/tmp/.X11-unix\
    -e DISPLAY=unix$DISPLAY \
    --device /dev/snd \
    --device /dev/video0 \
    --name $cname \
	$uid \
	$homefs \
	$image_name $bcmd

set +x
exit
